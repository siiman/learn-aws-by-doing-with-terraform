vpc = {
  ip   = "172.16.0.0/16"
  name = "VPC1"
}

subnets = {
  "public" = {
    ip                = "172.16.1.0/24",
    availability_zone = "us-east-1a",
    name              = "Public1",
    ingress_ports     = [22, 80]
  },
  "private" = {
    ip                = "172.16.2.0/24",
    availability_zone = "us-east-1b",
    name              = "Private1",
    ingress_ports     = [22]
  },
}

