variable "region" {
  default = "us-east-1"
}

variable "vpc" {
  type = map
}

variable "subnets" {
  type = map
}

