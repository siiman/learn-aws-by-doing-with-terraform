terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# Create a VPC
resource "aws_vpc" "main" {
  cidr_block = var.vpc.ip
  tags = {
    Name = var.vpc.name
  }
}

# Create a Public and Private Subnet in Different Availability Zones
resource "aws_subnet" "main" {
  for_each          = var.subnets
  vpc_id            = aws_vpc.main.id
  cidr_block        = each.value.ip
  availability_zone = each.value.availability_zone
  tags = {
    Name = each.value.name
  }
}

# Create Two Network Access Control Lists (NACLs), and Associate Each with the Proper Subnet
resource "aws_network_acl" "main" {
  for_each   = var.subnets
  vpc_id     = aws_vpc.main.id
  subnet_ids = [aws_subnet.main[each.key].id]

  dynamic "ingress" {
    for_each = toset(each.value.ingress_ports)
    content {
      protocol   = "tcp"
      rule_no    = 100 + ingress.key
      action     = "allow"
      cidr_block = each.key == "private" ? var.subnets.public.ip : "0.0.0.0/0"
      from_port  = ingress.value
      to_port    = ingress.value
    }
  }

  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 1024
    to_port    = 65535
  }

  tags = {
    Name = "${each.key}_NACL"
  }
}

# Create an Internet Gateway, and Connect It to the VPC
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "IGW"
  }
}

# Create Two Route Tables, and Associate Them with the Correct Subnet
resource "aws_route_table" "main" {
  for_each = var.subnets
  vpc_id   = aws_vpc.main.id
  tags = {
    Name = "${each.key}_RT"
  }
}

resource "aws_route_table_association" "main" {
  for_each       = var.subnets
  subnet_id      = aws_subnet.main[each.key].id
  route_table_id = aws_route_table.main[each.key].id
}

# Add internet gateway to public subnet route table
resource "aws_route" "main" {
  route_table_id         = aws_route_table.main["public"].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}
