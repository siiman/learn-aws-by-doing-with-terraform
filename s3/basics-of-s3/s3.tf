terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# Create simple private S3 bucket with versioning
resource "aws_s3_bucket" "private_bucky" {
  bucket = "v3ry0riginaln3wbucky"
  acl    = "private"
  versioning {
    enabled = true
  }
}

# Upload file to the priv bucket
resource "aws_s3_bucket_object" "priv_upload" {
  bucket = aws_s3_bucket.private_bucky.id
  key    = "important_file"
  source = "some_file.txt"
}

# Create a bucket with public-read policy
resource "aws_s3_bucket" "pub_by_policy" {
  bucket = "public-nic3-bucky"
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_policy" "pub_read" {
  bucket = aws_s3_bucket.pub_by_policy.id
  policy = <<EOF
{
  "Id": "Policy1601202942092",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1601206146344",
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.pub_by_policy.arn}/*",
      "Principal": "*"
    }
  ]
}
EOF
}

# Upload file to the pub bucket
resource "aws_s3_bucket_object" "pub_upload" {
  bucket = aws_s3_bucket.pub_by_policy.id
  key    = "ohnoez"
  source = "secretz.txt"
}