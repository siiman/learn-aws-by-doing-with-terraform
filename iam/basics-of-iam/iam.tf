terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# Workaround as per 
# https://www.terraform.io/docs/configuration/functions/flatten.html#flattening-nested-structures-for-for_each
# to add multiple policies with for_each loop later on
locals {
  group_policies = flatten([for group in var.groups : [
    for policy in group.policies : {
      group_name = group.name
      policy     = policy
    }]
  ])
}

# Create groups
resource "aws_iam_group" "main" {
  for_each = toset([for g in var.groups : g.name])
  name     = each.value
}

# Attach policies
resource "aws_iam_group_policy_attachment" "main" {
  for_each = {
    for g in local.group_policies : "${g.group_name}.${g.policy}" => g
  }
  group      = each.value.group_name
  policy_arn = each.value.policy
  # For some weird reason I got the occasional error NoSuchEntity: 
  # The group with name EC2-Admin cannot be found. status code: 404
  # with this. depends_on seems to fix it
  depends_on = [aws_iam_group.main]
}

# Create users 
resource "aws_iam_user" "main" {
  for_each = var.users
  name     = each.key
}

# Add them to appropriate groups
resource "aws_iam_user_group_membership" "main" {
  for_each = var.users
  user     = each.key
  groups   = each.value.groups
  # Same here
  depends_on = [aws_iam_group.main, aws_iam_user.main]
}
