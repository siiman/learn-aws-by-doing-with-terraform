variable "region" {
  default = "us-east-1"
}

variable "groups" {
  type = list
}

variable "users" {
  type = map
}