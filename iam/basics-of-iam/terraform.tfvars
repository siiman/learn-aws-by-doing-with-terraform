groups = [
  {
    name     = "EC2-Admin"
    policies = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess"]
  },
  {
    name     = "EC2-Support"
    policies = ["arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"]
  },
  {
    name     = "S3-Support"
    policies = ["arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"]
  }
]

users = {
  "johnc" = {
    groups = ["EC2-Admin"]
  },
  "markw" = {
    groups = ["EC2-Support"]
  },
  "anthonyh" = {
    groups = ["EC2-Support", "S3-Support"]
  }
}